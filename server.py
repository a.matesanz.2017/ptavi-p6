#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import os
import sys

# Argumentos

_, ADDRESS, PORTT, SONGG = sys.argv
PORTT = int(PORTT)

if len(sys.argv) != 4 or not os.path.exists(SONGG):

    print("Usage: python3 server.py IP port audio_file")



class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    def handle(self):

        # Escribe dirección y puerto del cliente (de tupla client_address)

        print("IP del cliente: " + str(self.client_address[0]))
        print("Puerto del cliente: " + str(self.client_address[1]))

        chainn = ""

        for line in self.rfile:

            ld = line.decode("utf-8")
            chainn = chainn + ld

        if chainn == "\r\n":

            print("No se han encontrado datos de cliente")

        else:

            print("El cliente nos manda:", chainn)
            chainn = chainn.split(" ")
            METHODD = chainn[0]
            address = chainn[1]
            sip = chainn[2]

            if METHODD == "INVITE":

                answer = b"SIP/2.0 100 Trying\r\n"
                answer = answer + b"SIP/2.0 180 Ringing\r\n"
                answer = answer + b"SIP/2.0 200 OK\r\n"
                self.wfile.write(answer)

            elif METHODD == "BYE":

                answer = b"SIP/2.0 200 OK\r\n"
                self.wfile.write(b"SIP/2.0 200 OK\r\n")

            elif METHODD == "ACK":

                S_ADDRESS = str(ADDRESS)
                exee = "mp32rtp -i " + S_ADDRESS + " -p 23032 < " + SONGG
                print("Vamos a ejecutar", exee)
                os.system(exee)

            elif METHODD != "INVITE" or "BYE" or "ACK":

                self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n")

            else:

                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n")



if __name__ == "__main__":

    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((ADDRESS, PORTT), EchoHandler)
    print("Listening...")
    serv.serve_forever()

