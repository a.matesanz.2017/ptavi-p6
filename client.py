#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

if len(sys.argv) != 3:

    print("Usage: python3 client.py method receiver@IP:SIPport")



# Argumentos

_, METHODD, ADRESSARG = sys.argv

ADDRESS = ADRESSARG.split(":")
ADDRESS = ADDRESS[0]

PORTT = ADRESSARG.split(":")
PORTT = PORTT[1]
PORTT = int(PORTT)

SERVER = ADDRESS.split("@")
SERVER = SERVER[1]



# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto.

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORTT))

    LINE = METHODD + " sip:" + ADDRESS + " SIP/2.0\r\n"
    print("Enviando:", LINE)
    my_socket.send(bytes(LINE, "utf-8") + b"\r\n")

    data = my_socket.recv(1024)
    answer = data.decode("utf-8").split()

    print("Recibido -- ")
    print(data.decode("utf-8"))

    codes = [answer[2]=="Trying", answer[5]=="Ringing", answer[8]=="OK"]

    MESSAGE = ("ACK sip:" + ADDRESS + " SIP/2.0")

    if METHODD == "INVITE" and all(codes):

        my_socket.send(bytes(MESSAGE, "utf-8") + b"\r\n")

    if METHODD == "BYE" and data.decode("utf-8") == "SIP/2.0 200 OK\r\n":

        print("Terminando socket...")

print("Fin.")

